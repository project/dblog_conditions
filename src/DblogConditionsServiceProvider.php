<?php

namespace Drupal\dblog_conditions;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;


class DblogConditionsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // replace DbLog by DbLogConditions by looking up the service definitions
    $container->setDefinition('logger.dblog', $container->getDefinition('logger.dblog_conditions'));
    $container->removeDefinition('logger.dblog_conditions');
  }
}
